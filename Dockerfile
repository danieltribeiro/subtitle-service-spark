FROM danieltribeiro/graalvm:ce-1.0.0-rc11-linux-amd64-gradle5.1.1
#FROM oracle/graalvm-ce:1.0.0-rc10
WORKDIR /graalvm
COPY . /graalvm
RUN /bin/gradle nativeImage --no-daemon

FROM alpine
WORKDIR /graalvm
COPY --from=0 /graalvm/app .
EXPOSE 8080
CMD ./app

# --rerun-class-initialization-at-runtime=jcifs.ntlmssp.Type3Message