package danieltribeiro.subtitle.spark;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GsonHelper {

    public static Boolean getAsBooleanOrNull(JsonObject jsonObject, String name) {
        if (jsonObject.get(name) == null) {
            return null;
          }
          return jsonObject.get(name).getAsBoolean();
      }

    public static String getAsStringOrNull(JsonObject jsonObject, String name) {
        if (jsonObject.get(name) == null) {
          return null;
        }
        return jsonObject.get(name).getAsString();
      }
    
      public static JsonArray getAsJsonArrayOrNull(JsonObject jsonObject, String name) {
        if (jsonObject.get(name) == null) {
          return null;
        }
        return jsonObject.get(name).getAsJsonArray();
      }
    
      public static JsonObject getAsJsonObjectOrNull(JsonElement jsonElement) {
        if (jsonElement == null) {
          return null;
        }
        return jsonElement.getAsJsonObject();
      }
    

}