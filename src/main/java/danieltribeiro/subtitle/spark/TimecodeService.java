package danieltribeiro.subtitle.spark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TimecodeService {
  
  private Gson gson = App.getGson();
  
  private static final String DEFAULT_BASE_URL = "http://localhost:8080";

  private MyHttpClient httpClient;

  public TimecodeService() {
    String baseUrl = System.getenv("TIMECODE_SERVICE_URL");
    if (baseUrl == null) {
      baseUrl = DEFAULT_BASE_URL;
    }
    this.httpClient = new MyHttpClient();
    httpClient.setBaseUrl(baseUrl);
  }

  public TimecodeService(String baseUrl) {
    this.httpClient = new MyHttpClient(baseUrl);
  }


  /**
   * Convert a lista of timecodes framerates on the format 00:00:00:00
   * @param srcfr
   * @param destfr
   * @param offset
   * @param timecodes
   * @return
   * @throws Exception
   */

  public Map<String, String> convertTimecodes(String srcfr, String destfr, String offset, List<String> timecodes)
            throws Exception {
    String body = new Gson().toJson(timecodes);
    String path = "/convert/" + srcfr + "/" + destfr + "/" + offset + "/asMap";
    Map<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json;charset=UTF-8");
    String string = httpClient.execute("POST", path, headers, body.getBytes());

    JsonObject map = new JsonParser().parse(string).getAsJsonObject();
    Map<String, String> result = new HashMap<>();

    for (Entry<String, JsonElement> e : map.entrySet()) {
      result.put(e.getKey(), e.getValue().getAsJsonObject().get("string").getAsString());
    }
    return result;
  }

  /**
   * Convert a list of timestamps framerates in the format 00:00:00.000
   * @param srcfr
   * @param destfr
   * @param offset
   * @param timecodes
   * @return
   * @throws Exception
   */
  public Map<String, String> convertSrtTimestamps(String srcfr, String destfr, String offset, List<String> timecodes)
            throws Exception {
    String body = gson.toJson(timecodes);
    String path = "/convert/" + srcfr + "/" + destfr + "/" + offset.replaceAll(",", ".") + "/asMap";
    Map<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json;charset=UTF-8");
    String string = httpClient.execute("POST", path, headers, body.getBytes());

    JsonObject map = new JsonParser().parse(string).getAsJsonObject();
    Map<String, String> result = new HashMap<>();

    for (Entry<String, JsonElement> e : map.entrySet()) {
      result.put(e.getKey(), e.getValue().getAsJsonObject().get("timestamp").getAsString().replaceAll("\\.", ","));
    }
    return result;
  }

  
}