package danieltribeiro.subtitle.spark;

import static spark.Spark.get;
import static spark.Spark.post;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.MultipartConfigElement;

import com.google.gson.Gson;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import danieltribeiro.subtitle.SrtFormat;
import danieltribeiro.subtitle.SubFormat;
import danieltribeiro.subtitle.Subtitle;
import danieltribeiro.subtitle.SubtitleEntry;
import io.prometheus.client.Counter;
import io.prometheus.client.exporter.MetricsServlet;

public class SubtitleController {

    Logger log = Log.getLogger(SubtitleController.class);

    static final Counter requests = Counter.build().name("requests_total").help("Total requests").register();

    private TimecodeService timecodeService;

    public SubtitleController(TimecodeService timecodeService) {

        this.timecodeService = timecodeService;

        Gson gson = App.getGson();
        get("/metrics", (req, res) -> {
            // return requests.toString();
            return new MetricsServlet();
        });

        post("/convert-and-fix/sub/:srcfr/:destfr", (req, res) -> {
            requests.inc();

            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            Subtitle subtitle;
            try (InputStream inputStream = req.raw().getPart("file").getInputStream()) {
                subtitle = parse(req.params("srcfr"), inputStream);
            } catch (Exception e) {
                throw e;
            }

            fixSub(req.params("srcfr"), req.params("destfr"), subtitle);

            String text = format(subtitle);
            byte[] bytes = text.getBytes(StandardCharsets.ISO_8859_1);
            res.type("text/plain;charset=ISO-8859-1");
            return bytes;
        });

        post("/convert-and-fix/srt/:srcfr/:destfr", (req, res) -> {
            requests.inc();

            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            Subtitle subtitle;
            try (InputStream inputStream = req.raw().getPart("file").getInputStream()) {
                subtitle = parseSrt(req.params("srcfr"), inputStream);
            } catch (Exception e) {
                throw e;
            }

            fixSrt(req.params("srcfr"), req.params("destfr"), subtitle);

            String text = formatSrt(subtitle);
            byte[] bytes = text.getBytes(StandardCharsets.ISO_8859_1);
            res.type("text/plain;charset=ISO-8859-1");
            return bytes;
        });

        post("/convert/:destfr", (req, res) -> {
            requests.inc();
            Subtitle subtitle = gson.fromJson(req.body(), Subtitle.class);
            String offset = "0";
            if (subtitle.firstVisibleEntry() != null) {
                offset = subtitle.firstVisibleEntry().getStart();
            }
            res.type("application/json");
            return convertFrameRate(req.params("destfr"), offset, subtitle);
        }, gson::toJson);

        post("/convert/:destfr/:offset", (req, res) -> {
            requests.inc();

            Subtitle subtitle = gson.fromJson(req.body(), Subtitle.class);
            res.type("application/json");
            return convertFrameRate(req.params("destfr"), req.params("offset"), subtitle);
        }, gson::toJson);

        post("/format/sub", (req, res) -> {
            requests.inc();
            Subtitle subtitle = gson.fromJson(req.body(), Subtitle.class);
            String text = format(subtitle);
            byte[] bytes = text.getBytes(StandardCharsets.ISO_8859_1);
            res.type("text/plain;charset=ISO-8859-1");
            return bytes;
        });

        post("/format/srt", (req, res) -> {
            requests.inc();
            Subtitle subtitle = gson.fromJson(req.body(), Subtitle.class);
            String text = formatSrt(subtitle);
            byte[] bytes = text.getBytes(StandardCharsets.ISO_8859_1);
            res.type("text/plain;charset=ISO-8859-1");
            return bytes;
        });

        post("/parse/sub/:fr", (req, res) -> {
            requests.inc();
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            try (InputStream inputStream = req.raw().getPart("file").getInputStream()) {
                res.type("application/json");
                return parse(req.params("fr"), inputStream);
            } catch (Exception e) {
                throw e;
            }
        }, gson::toJson);

        post("/parse/srt/:fr", (req, res) -> {
            requests.inc();
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            try (InputStream inputStream = req.raw().getPart("file").getInputStream()) {
                res.type("application/json");
                return parseSrt(req.params("fr"), inputStream);
            } catch (Exception e) {
                throw e;
            }
        }, gson::toJson);

    }

    public Subtitle fixSub(String srcfr, String destfr, Subtitle subtitle) throws Exception {
        String offset = "0";
        if (subtitle.firstVisibleEntry() != null) {
            offset = subtitle.firstVisibleEntry().getStart();
        }

        convertFrameRate(destfr, offset, subtitle);

        if (subtitle.getHeader() == null) {
            subtitle.setHeader("*PART 1*\r\n00:00:00:00\\00:00:00:00");
        }
        if (subtitle.getFooter() == null) {
            String lastTC = "00:00:000:00";
            if (subtitle.lastEntry() != null) {
                lastTC = subtitle.lastEntry().getEnd();
            }
            subtitle.setFooter("*END PART 1*\r\n" + lastTC + "\\" + lastTC + "\r\n*CODE*\r\n0000000000000000\r\n");
        }
        List<SubtitleEntry> entries = subtitle.getEntries();
        for (SubtitleEntry e : entries) {
            String[] lines = Arrays.stream(e.getLines()).map(s -> s.replaceAll("\\[\\]", "[")).toArray(String[]::new);
            e.setLines(lines);
        }

        return subtitle;
    }

    public Subtitle fixSrt(String srcfr, String destfr, Subtitle subtitle) throws Exception {
        String offset = "0";
        if (subtitle.firstVisibleEntry() != null) {
            offset = subtitle.firstVisibleEntry().getStart();
        }

        List<SubtitleEntry> entries = subtitle.getEntries();
        for (SubtitleEntry e : entries) {
            e.setStart(e.getStart().replaceAll("\\,", "."));
            e.setEnd(e.getEnd().replaceAll("\\,", "."));
        }

        convertFrameRate(destfr, offset, subtitle);

        for (SubtitleEntry e : entries) {
            e.setStart(e.getStart().replaceAll("\\.", ","));
            e.setEnd(e.getEnd().replaceAll("\\.", ","));
        }

        return subtitle;
    }

    public Subtitle convertFrameRate(String destfr, Object offset, Subtitle subtitle) throws Exception {
        String srcfr = subtitle.getFrameRate();
        subtitle.setFrameRate(destfr);
        ArrayList<String> sourcetimecodes = new ArrayList<String>();
        if (subtitle.getEntries() == null) {
            // No entry to convert, just return the same subtitle
            return subtitle;
        }

        for (SubtitleEntry entry : subtitle.getEntries()) {
            sourcetimecodes.add(entry.getStart());
            sourcetimecodes.add(entry.getEnd());
        }
        Map<String, String> destTimecodes;
        
        if (subtitle.getOriginalFormat() == null) {
            destTimecodes = timecodeService.convertTimecodes(srcfr, destfr, offset.toString(), sourcetimecodes);
        } else if (subtitle.getOriginalFormat().equals("SRT")) {
            destTimecodes = timecodeService.convertSrtTimestamps(srcfr, destfr, offset.toString(), sourcetimecodes);
        } else {
            destTimecodes = timecodeService.convertTimecodes(srcfr, destfr, offset.toString(), sourcetimecodes);
        }
                
        for (SubtitleEntry entry : subtitle.getEntries()) {
            entry.setStart(destTimecodes.get(entry.getStart()));
            entry.setEnd(destTimecodes.get(entry.getEnd()));
        }
        return subtitle;
    }

    public Subtitle convertFrameRate(String destfr, Subtitle subtitle) throws Exception {
        String offset = "0";
        if (subtitle.firstEntry() != null) {
            offset = subtitle.firstEntry().getStart();
        }
        if (offset == null) {
            offset = "0";
        }
        return convertFrameRate(destfr, offset, subtitle);
    }

    public Subtitle parse(String fr, InputStream inputStream) throws IOException {
        String text = readInputStream(inputStream, StandardCharsets.ISO_8859_1);
        return parse(fr, text);
    }

    public Subtitle parse(String fr, String text) {
        Subtitle s = new SubFormat().parse(text);
        s.setFrameRate(fr);
        return s;
    }

    public Subtitle parseSrt(String fr, InputStream inputStream) throws IOException {
        String text = readInputStream(inputStream, StandardCharsets.UTF_8);
        return parseSrt(fr, UtfBomHelper.removeUTF8BOM(text));
    }

    private String readInputStream(InputStream inputStream, Charset charset) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];

        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        byte[] byteArray = buffer.toByteArray();
        String text = new String(byteArray, charset);
        inputStream.close();
        return text;
    }

    public Subtitle parseSrt(String fr, String text) {
        Subtitle s = new SrtFormat().parse(text);
        s.setFrameRate(fr);
        return s;
    }

    public String format(Subtitle subtitle) {
        return new SubFormat().format(subtitle);
    }

    public String formatSrt(Subtitle subtitle) {
        return new SrtFormat().format(subtitle);
    }
}
