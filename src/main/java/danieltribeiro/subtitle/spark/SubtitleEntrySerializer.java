package danieltribeiro.subtitle.spark;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import danieltribeiro.subtitle.SubtitleEntry;

import static danieltribeiro.subtitle.spark.GsonHelper.*;

public class SubtitleEntrySerializer implements JsonSerializer<SubtitleEntry>, JsonDeserializer<SubtitleEntry> {

    @Override
    public JsonElement serialize(SubtitleEntry src, java.lang.reflect.Type typeOfSrc,
            JsonSerializationContext context) {
        JsonObject json = new JsonObject();

        json.addProperty("start", src.getStart());
        json.addProperty("end", src.getEnd());
        json.addProperty("params", src.getParams());
        json.addProperty("visibleContent", src.isVisibleContent());
        JsonArray lines = new JsonArray();
        if (src.getLines() != null) {
            for (int i = 0; i < src.getLines().length; i++) {
                lines.add(src.getLines()[i]);
            }
            json.add("lines", lines);
        }
        return json;
    }

    @Override
    public SubtitleEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        SubtitleEntry se = new SubtitleEntry();
        JsonObject jObject = json.getAsJsonObject();

        se.setStart(getAsStringOrNull(jObject, "start"));
        se.setEnd(getAsStringOrNull(jObject, "end"));
        se.setContent(getAsStringOrNull(jObject, "content"));
        se.setParams(getAsStringOrNull(jObject, "params"));
        se.setVisibleContent(getAsBooleanOrNull(jObject, "visibleContent"));

        JsonArray linesJsonArry = getAsJsonArrayOrNull(jObject, "lines");

        if (linesJsonArry.size() > 0) {
            List<String> lines = new ArrayList<>();
            for (int i = 0; i < linesJsonArry.size(); i++) {
                lines.add(linesJsonArry.get(i).getAsString());
            }
            se.setLines(lines.toArray(new String[0]));
        }

        return se;
    }
}