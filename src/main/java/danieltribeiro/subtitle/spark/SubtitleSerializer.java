package danieltribeiro.subtitle.spark;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import danieltribeiro.subtitle.Subtitle;
import danieltribeiro.subtitle.SubtitleEntry;

import static danieltribeiro.subtitle.spark.GsonHelper.*;

public class SubtitleSerializer implements JsonSerializer<Subtitle>, JsonDeserializer<Subtitle> {

  @Override
  public JsonElement serialize(Subtitle src, java.lang.reflect.Type typeOfSrc, JsonSerializationContext context) {
    JsonObject json = new JsonObject();

    json.addProperty("name", src.getName());
    json.addProperty("originalFormat", src.getOriginalFormat());
    json.addProperty("frameRate", src.getFrameRate());
    json.addProperty("header", src.getHeader());
    json.addProperty("footer", src.getFooter());

    JsonArray entries = new JsonArray();
    SubtitleEntrySerializer entrySerializer = new SubtitleEntrySerializer();
    if (src.getEntries() != null) {
      for (SubtitleEntry e : src.getEntries()) {
        entries.add(entrySerializer.serialize(e, typeOfSrc, context));
      }
      json.add("entries", entries);
    }

    return json;
  }

  @Override
  public Subtitle deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
        JsonObject jobject = json.getAsJsonObject();

        Subtitle subtitle = new Subtitle();
        subtitle.setOriginalFormat(getAsStringOrNull(jobject, "originalFormat"));
        subtitle.setFrameRate(getAsStringOrNull(jobject, "frameRate"));

        JsonArray entries = getAsJsonArrayOrNull(jobject, "entries");
        
        if (entries != null) {
          subtitle.setEntries(new ArrayList<>());

          int entryCount = entries.size();
          for (int i = 0; i < entryCount; i++) {
            JsonObject eObject = getAsJsonObjectOrNull(entries.get(i));
            if (eObject != null) {
              SubtitleEntry se = App.getGson().fromJson(eObject, SubtitleEntry.class);
              subtitle.getEntries().add(se);
            }
          }
        }

        return subtitle;
  }
}