package danieltribeiro.subtitle.spark;

import java.util.Map;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.util.BytesContentProvider;
import org.eclipse.jetty.util.ssl.SslContextFactory;

public class MyHttpClient {

    private String baseUrl;

    public MyHttpClient() {
    }

    public MyHttpClient(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @return the baseUrl
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl the baseUrl to set
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String execute(String method, String path, Map<String, String> headers, byte[] body) throws Exception {
        HttpClient httpClient = new HttpClient(new SslContextFactory(true));
        httpClient.start();
        Request req = httpClient.newRequest(baseUrl).method(method).path(path);
        for (Map.Entry<String, String> e : headers.entrySet()) {
            req.header(e.getKey(), e.getValue());
        }
        if (body != null) {
            req.content(new BytesContentProvider(body));
        }
        ContentResponse res = req.send();
        httpClient.stop();
        return res.getContentAsString();
    }
}